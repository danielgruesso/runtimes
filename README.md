# Knative runtimes

Knative runtimes are also known as [Build Templates](https://github.com/knative/build-templates), they are used to build container images for your functions.

The [kaniko](https://github.com/GoogleContainerTools/kaniko) runtime is adapted from the [original one](https://github.com/knative/build-templates/tree/master/kaniko) to provide additional configuration.

The Nodejs runtime is based on Pivotal [Riff project](https://github.com/projectriff/riff)

## Support

If you encounter issues with these runtimes please create an issue as best as we can.